using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMovement : MonoBehaviour
{

private SpriteRenderer mySprite;

public Sprite idleSprite;
public Animator animator;

public float speed;
private int dir = 1;

private float animSpeed;

private float inputX = 0;
private float inputY = 0;

private Transform transf;
private Transform myTransform
{
    get

    {
if (transf == null){
transf = this.transform;
}
        return transf;
    }
}

private RectTransform rect = null;
private RectTransform rectTransform
{
    get
    {
if(rect == null)
rect = this.GetComponent<RectTransform>();

return rect;
    }
}

private void Start()
{
    mySprite = this.GetComponent<SpriteRenderer>();
}

private void Update(){
    Movement();
}

    private void Movement()
    {
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");

        if(myTransform.position.x > 6 && inputX > 0)
        inputX = 0;
        else if(myTransform.position.x < -6 && inputX < 0)
        inputX = 0;

        if(myTransform.position.y > 4 && inputY > 0)
        inputY = 0;
        else if(myTransform.position.y < -4 && inputY < 0)
        inputY = 0;

        dir = (int)inputX;

        myTransform.Translate(Vector2.right * inputX * speed * Time.deltaTime);
        myTransform.Translate(Vector2.up * inputY * speed * Time.deltaTime);

        float scaleX = dir != 0 ? dir : 1;
        myTransform.localScale = new Vector2(scaleX, 1);

        Sprite currentSPrite = mySprite.sprite;

        if(animSpeed != 0)
        animator.SetBool("flying", true);
        else 
            animator.SetBool("flying", (currentSPrite.name != "passaro_14"));

        animSpeed = Mathf.MoveTowards(animSpeed, Mathf.Abs(speed), Time.deltaTime);
        animSpeed = Mathf.Clamp(animSpeed, 0, 2);
        animator.speed = animSpeed != 0 ? animSpeed : (currentSPrite.name != "passaro_14") ? .5f : animSpeed;


    }
}